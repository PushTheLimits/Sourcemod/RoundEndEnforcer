#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <cstrike>
#include <autoexecconfig>
#include <colors>

#pragma newdecls required


// Config handles
Handle c_timeLimit;
Handle c_remainingAnnaunceHighValue;
Handle c_remainingAnnaunceHighTick;
Handle c_remainingAnnaunceLowValue;
Handle c_remainingAnnaunceLowTick;
Handle c_remainingAnnounceCriticalValue;

// Timers
Handle g_timer;

// Members
int currentTimeRemaining;
int remainingAnnaunceHighValue;
int remainingAnnaunceHighTick;
int remainingAnnaunceLowValue;
int remainingAnnaunceLowTick;
int remainingAnnounceCriticalValue;

int currentStageTick = 0;

char PLUGIN_PREFIX[] = "RoundEndEnforcer";

public Plugin myinfo =
{
	name = "RoundEndEnforcer",
	author = "Kevin 'RAYs3T' Urbainczyk",
	description = "Ends a round after specified time by killing the players",
	version = "2.0",
	url = "https://ptl-clan.de"
};

public void OnPluginStart()
{
	AutoExecConfig_SetFile("RoundEndEnforcer", "ptl");

	c_timeLimit = AutoExecConfig_CreateConVar("ree_limit", "840", "Seconds after that the round should end. -1=disabled, 60=1 minute, \
300=5 minutes, 720=12 minutes. and so on ...");
	
	c_remainingAnnaunceHighValue 	 = AutoExecConfig_CreateConVar("ree_announce_high_value", "300", 
		"The time (seconds) from when the messages should be displayed");
		
	c_remainingAnnaunceHighTick  	 = AutoExecConfig_CreateConVar("ree_announce_high_tick",  "60", 
		"The frequency (tickrate) for displaying the messages while in the high announce level");
		
	c_remainingAnnaunceLowValue  	 = AutoExecConfig_CreateConVar("ree_announce_low_value",  "120", 
		"The time (seconds) from when the second layer of messages should kick in. This will also show an hint and not only the message");
		
	c_remainingAnnaunceLowTick   	 = AutoExecConfig_CreateConVar("ree_announce_low_tick",  "30",  
		"The frequency (tickrage) for displaying the messages and hints, while in the low announce level.");
		
	c_remainingAnnounceCriticalValue = AutoExecConfig_CreateConVar("ree_announce_critical_value",  "10", 
		"The time (seconds) from when the critical messages should kick in. Those like the same from the low level, but showing EVERY tick. \
These are indended to be used for a more dramatic effect on your players, when the time is getting realy short. frighten them!");
		

	AutoExecConfig_CleanFile();
	AutoExecConfig(true, "RoundEndEnforcer", "ptl");
	
	RegAdminCmd("ree_disable", Command_Disable, ADMFLAG_KICK, "Stops the RoundEndEnforcer for this round. \
After the next round is starting, the timer is running again");
	
	HookEvent("round_start", RoundStart);
	HookEvent("round_end", RoundEnd);
}

public void RoundStart(Handle event, const char[] name, bool dontBroadcast)
{
	// Initialize configs to local members, keep the ability to change all those settings without restarting
	currentTimeRemaining 			= GetConVarInt(c_timeLimit);
	remainingAnnaunceHighValue		= GetConVarInt(c_remainingAnnaunceHighValue);
	remainingAnnaunceHighTick		= GetConVarInt(c_remainingAnnaunceHighTick);
	remainingAnnaunceLowValue		= GetConVarInt(c_remainingAnnaunceLowValue);
	remainingAnnaunceLowTick		= GetConVarInt(c_remainingAnnaunceLowTick);
	remainingAnnounceCriticalValue	= GetConVarInt(c_remainingAnnounceCriticalValue);
	
	// Check if the plugin was disabled
	if(currentTimeRemaining == -1){
		PrintToServer("[%s] The plugin is currently disabled because ree_limit is set to -1", PLUGIN_PREFIX);
		return;
	}
	// Create a timer that is checking each second
	g_timer = CreateTimer(1.00, checkLoop, INVALID_HANDLE, TIMER_REPEAT);
}

public void RoundEnd(Handle event, const char[] name, bool dontBroadcast)
{
	g_timer = INVALID_HANDLE;
}

// Aborts the current execution of the timer for this round.
public Action Command_Disable(int client, int args){
	g_timer = INVALID_HANDLE;
	CPrintToChatAll("{green}[RoundEndEnforcer]{lightgreen} is now disabeld for this Round");
	
}

public Action checkLoop(Handle timer, any data){
	if(g_timer == INVALID_HANDLE){
		// Timer was interrupted, stop further execution.
		return Plugin_Stop;
	}
	
	// Well finally, when the time is over, i think you can guess it ...
	if(currentTimeRemaining == 0){
		killAllPlayer();
	}
	else if(currentTimeRemaining <= remainingAnnaunceLowValue) {
		// Inside critical or low ticks
		if(currentTimeRemaining <= remainingAnnounceCriticalValue || currentStageTick <= 0){
			// For low ticks, we want to display both messages. One in the chat, the other as an hint.
			printTimeMessage(currentTimeRemaining);
			printHintTimeMessage(currentTimeRemaining);
			currentStageTick = remainingAnnaunceLowTick;
		}
	}
	else if(currentTimeRemaining <= remainingAnnaunceHighValue){
		// Inside high ticks
		if(currentStageTick <= 0){
			// For high ticks we just want to show the message in the chat.
			printTimeMessage(currentTimeRemaining);
			currentStageTick = remainingAnnaunceHighTick;
		}
	}

	currentStageTick--;
	currentTimeRemaining--;
	return Plugin_Continue;
}

public void printTimeMessage(int time){
	char msg[128];
	// If the time is below 60 this means we just have to display the seconds
	
	int minutes = 0;
	for (int i = 60; i < time; i += 60) {
		minutes++;
	} 
	
	int seconds = time - (minutes * 60);
	if(minutes > 0 && seconds == 60) {
		minutes++;
		seconds = 0;
	}
	
	
	if(isLessThenAMinute(time)){
		Format(msg, sizeof(msg), "{green}[%s] {default}%i{lightgreen} seconds left ...", PLUGIN_PREFIX, time);
	}else{
		Format(msg, sizeof(msg), "{green}[%s] {default}%i{lightgreen} minutes {default}%i{lightgreen} seconds left ...", PLUGIN_PREFIX, minutes, seconds);
	}
	
	CPrintToChatAll("%s", msg);
}

public void printHintTimeMessage(int time){
	PrintHintTextToAll("%d seconds left...", time);
}


public void killAllPlayer(){
	// Loop trough all players and kill themh
	for (int i = 1; i < MaxClients + 1; i++)
	{
		if (IsClientInGame(i) && IsPlayerAlive(i)){
			ForcePlayerSuicide(i);
		}
	}
}

public bool isLessThenAMinute(int seconds){
	return seconds <= 60;
}
