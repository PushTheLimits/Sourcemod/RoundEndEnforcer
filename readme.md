# Sourcemod RoundEndEnforcer plugin for HL2-Engine-Games
## Description
### The basic functionality
This plugin adds a round timer to the game. If you playing some game modes like Jail or surf, this would fit perfectly, to limit endless rounds.
You may have noticed that those rounds will never ends, when there are no objective goals (like hostages or bombs) on the map.

To solve this problem, i've written this tiny plugin. It basically adds messages and the timer itself, 
together with some configuratable properties.

### Some fancy messages for feedback
Well, it would be okay to just set the time, waint until it's over end then end the round, this is not what is expected for most cases.
So, while you have the ability to disable the messages, there are some configuration properties to adjust them.
Just take a look at the configuration section.

## Configuration
### Path of configuration
this plugin uses [AutoExecConfig](https://github.com/Impact123/AutoExecConfig) for the configuration files.
The files are located in `<MainGameFolderOnServer>/cfg/ptl/RoundEndEnforcer.cfg`.


### Properties
Inside this config are some tweekable options, as described below

#### Values
All time values (`_value`) are defined in seconds.
So for examle one minute equals `60`, three minutes `180` and five minutes `300`.

The values defining the point from when this kind of messages should be displayed.
If you set it to `60` this will mean the messages for this group will start when 60 seconds are remaining.

#### Ticks
The tick configuration (`_tick`) are the amount of seconds betwenn each message is shown.
For example if you set the tickrate to `5`, this will mean one message is displayed every five seconds.

#### Variables
##### First message level (high)
`ree_announce_high_tick` seconds betwenn the messages
`ree_announce_high_value` seconds remaining before the first messages, that should inform the player about the remaining time, should be displayed.

##### Second message level (low)
`ree_announce_low_tick` seconds between the messages for the second (more close to the end) time time period
`ree_announce_low_value` Time from when the more frequent announcements should happen

##### Critical message level
`ree_announce_critical_value` This is for the last dramatically seconds, before the round will be forced to end. Like the last 10 seconds remaining ...

##### General round time limit
`ree_limit` defines the complete time, that is available for this round (in seconds).


## Ingame commands
### Disable the timer for one round
If you want to disable the timer just for one round, you can easly do it by entering the command `ree_disable` into your console.
The timer will then start again in the next round.


# Downloads
To download the plugin as full package just use the download arrow in the upper right cornor and select download under "Artifacts".
Or just klick on this [link](https://gitlab.com/PushTheLimits/Sourcemod/RoundEndEnforcer/-/jobs/artifacts/master/download?job=build).